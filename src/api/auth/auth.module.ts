import { Module } from '@nestjs/common';
import { ErrorsService } from '../../common/error.service';
import { AppJwtModule } from '../../jwt';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
  imports: [
    AppJwtModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, ErrorsService],
  exports: [AuthService],
})
export class AuthModule {
}
