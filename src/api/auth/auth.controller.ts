import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginInputDto } from './dto/login-input.dto';
import { RegisterInputDto } from './dto/register-input.dto';
import { LoginResponseDto } from './dto/login-response.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  public login(
    @Body() { email, password }: LoginInputDto,
  ): Promise<LoginResponseDto> {
    return this.authService.authenticate(email, password);
  }

  @Post('register')
  public register(
    @Body() registerInputDto: RegisterInputDto,
  ): Promise<LoginResponseDto> {
    return this.authService.register(registerInputDto);
  }
}
