import { TokenDto } from './token.dto';

export class LoginResponseDto {
  public id: number;
  public firstName?: string;
  public lastName?: string;
  public email: string;
  public tokens: TokenDto;
}
