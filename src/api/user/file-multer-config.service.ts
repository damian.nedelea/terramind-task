import { Injectable } from '@nestjs/common';
import {
  MulterModuleOptions,
  MulterOptionsFactory,
} from '@nestjs/platform-express';
import { existsSync, mkdirSync } from 'fs';

import { diskStorage } from 'multer';
import {
  editFileName,
  getStoragePathByUploadTarget,
} from '../../core/upload.utils';

@Injectable()
export class FileMulterConfigService implements MulterOptionsFactory {
  public createMulterOptions(): MulterModuleOptions {
    return {
      limits: {
        fileSize: 20 * 1024 * 1024,
        fieldSize: 20 * 1024 * 1024,
      },
      storage: diskStorage({
        destination: (req, file, callback) => {
          const newDestination = getStoragePathByUploadTarget(
            req.params.productId,
          );
          if (!existsSync(newDestination.longPath)) {
            mkdirSync(newDestination.longPath, { recursive: true });
          }
          callback(null, newDestination.longPath);
        },
        filename: editFileName,
      }),
    };
  }
}
