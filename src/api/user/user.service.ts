import { Injectable } from '@nestjs/common';
import { IAuthUser } from '../../core/auth';
import { UserEntity } from './entity/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { FileEntity } from '../file/entity/file.entity';
import { FileService } from '../file/file.service';

@Injectable()
export class UserService {
  constructor(
    private readonly fileService: FileService,
    @InjectRepository(UserEntity)
    private readonly userRepo: Repository<UserEntity>,
    @InjectRepository(FileEntity)
    private readonly fileRepo: Repository<FileEntity>,
  ) {}

  public async uploadFile(authUser: IAuthUser, file): Promise<object> {
    const newFile = {
      originalName: file.originalname,
      path: `user/${authUser.id}/images/${file.filename}`,
      mimetype: file.mimetype,
    };
    file.path = newFile.path;

    await this.fileRepo.save(newFile);

    return newFile;
  }

  public async deleteFile(
    authUser: IAuthUser,
    fileId: number,
    skipUpdate = false,
  ): Promise<boolean> {
    const dbFile = await this.fileRepo.findOne({
      where: {
        id: fileId,
        user_id: authUser.id,
      },
    });

    if (!skipUpdate) {
      await this.fileRepo.delete({
        id: fileId,
        user_id: authUser.id,
      });
    }

    await this.fileService.deleteLocalFile(dbFile.path);
    return true;
  }
}
