import {
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';

import { FileInterceptor } from '@nestjs/platform-express';
import { AUTH_GUARD, AuthUser, IAuthUser } from '../../core/auth';
import { getStoragePath } from '../../core/upload.utils';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/:productId/images/:fileId')
  public async sendImageUrl(
    @Param('fileId') fileId: string,
    @Param('productId') productId: string,
    @Res() res,
  ): Promise<void> {
    res.sendFile(fileId, {
      root: getStoragePath(`/product/storage/images`).longPath,
    });
  }

  @UseGuards(AUTH_GUARD)
  @Post('/upload')
  @UseInterceptors(FileInterceptor('file'))
  public async uploadFile(
    @UploadedFile() file,
    @AuthUser() authUser: IAuthUser,
  ): Promise<object> {
    return this.userService.uploadFile(authUser, file);
  }

  @UseGuards(AUTH_GUARD)
  @Delete('/:fileId')
  public async deleteFile(
    @AuthUser() authUser: IAuthUser,
    @Param('fileId', new ParseIntPipe()) fileId: number,
  ): Promise<boolean> {
    return this.userService.deleteFile(authUser, fileId, false);
  }
}
