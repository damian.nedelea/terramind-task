import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';

export class FileCreateDto {
  @IsString()
  @MaxLength(100)
  @IsNotEmpty()
  public originalName?: string;

  @IsString()
  @MaxLength(200)
  @IsNotEmpty()
  public path?: string;

  @IsString()
  @MaxLength(100)
  @IsNotEmpty()
  public mimetype?: string;

  @IsString()
  @IsOptional()
  public description?: string;

}
