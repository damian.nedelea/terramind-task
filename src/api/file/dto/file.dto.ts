export class FileDto {
  public id?: number;
  public originalName?: string;
  public path?: string;
  public mimetype?: string;
  public description?: string;
}
