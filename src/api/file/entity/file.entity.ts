import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { AbstractEntity } from '../../../common/abstraction/entity/abstract.entity';
import { UserEntity } from '../../user/entity/user.entity';

@Entity({ name: 'file' })
export class FileEntity extends AbstractEntity {
  @Column('varchar', { name: 'original_name', length: 100 })
  public originalName: string;
  @Column('varchar', { name: 'path', length: 200, unique: false })
  public path: string;
  @Column('varchar', { name: 'mimetype', length: 100 })
  public mimetype: string;
  @Column('text', { name: 'description', nullable: true })
  public description: string;

  @Column('int', { name: 'product_id', select: false, nullable: true })
  public user_id: number;

  @ManyToOne(() => UserEntity, (e) => e.files)
  @JoinColumn([{ name: 'user_id' }])
  public user: UserEntity;
}
