import { Injectable, Logger } from '@nestjs/common';
import * as fs from 'fs';
import { getStoragePath } from '../../core/upload.utils';

@Injectable()
export class FileService {
  private readonly logger = new Logger(FileService.name);

  public async deleteLocalFile(path: string): Promise<void> {
    if (path) {
      path = getStoragePath(path).longPath;
      this.logger.debug('Delete local file: ' + path);
      if (fs.existsSync(path)) {
        this.logger.debug('Local file exists, removing...');
        return this.removeFile(path);
      } else {
        this.logger.debug('Local file not found... skip.');
        return undefined;
      }
    } else {
      return undefined;
    }
  }
  private removeFile(path: string): void {
    fs.unlink(path, (err) => {
      if (err) {
        this.logger.error('Failed to delete local file:' + err);
      } else {
        this.logger.error('Successfully deleted local file');
      }
    });
  }
}
